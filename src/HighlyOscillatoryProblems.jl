module HighlyOscillatoryProblems
include("interface.jl")
export HiOscODEProblem, HiOscInterpolation, AbstractHiOscSolution, HiOscODESolution
export solve, getexactsol
end # module
